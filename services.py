import sqlite3

def insert_post(username, message):
    with sqlite3.connect('app.db') as conn:
        sql = "INSERT INTO POST(username, message) VALUES(?, ?)"
        cursor = conn.cursor()
        cursor.execute(sql, (username, message))
        return cursor.lastrowid

def get_posts():
    with sqlite3.connect('app.db') as conn:
        sql = "SELECT * FROM POST ORDER BY TIMESTAMP DESC"
        cursor = conn.cursor()
        result = cursor.execute(sql)
        desc = cursor.description
        column_names = [col[0] for col in desc]
        data = [dict(zip(column_names, row))
            for row in cursor.fetchall()]
        return data
