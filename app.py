from flask import Flask, render_template, request, jsonify
import services

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        username = request.form['username']
        message = request.form['message']
        services.insert_post(username, message)
    posts = services.get_posts()
    print(posts)
    return render_template('index.html', posts=posts)

@app.route('/api/posts')
def api_posts():
    posts = services.get_posts()
    return jsonify(posts)

if __name__ == '__main__':
    app.run(debug=True)