import sqlite3

conn = sqlite3.connect('../app.db')

with open('CREATE_POST_TABLE.sql') as f:
    sql = f.read()
    cursor = conn.cursor()
    cursor.execute(sql)

conn.close()