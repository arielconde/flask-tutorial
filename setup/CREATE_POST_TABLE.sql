CREATE TABLE POST (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    username TEXT NOT NULL,
    message TEXT NOT NULL,
    timestamp DATE DEFAULT (datetime('now','localtime'))
)